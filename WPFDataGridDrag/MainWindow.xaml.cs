﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridDrag
{
    /**
     * @brief パーソンクラス
     *        データグリッドに表示するクラス
     */
    class Person
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドにサンプルデータを設定します。
            var list = new List<Person>();
            list.Add(new Person() { Name = "佐藤", Address = "東京", Age = 20 });
            list.Add(new Person() { Name = "鈴木", Address = "千葉", Age = 31 });
            list.Add(new Person() { Name = "高橋", Address = "埼玉", Age = 42 });
            list.Add(new Person() { Name = "田中", Address = "神奈川", Age = 53 });
            list.Add(new Person() { Name = "伊藤", Address = "群馬", Age = 64 });
            dataGrid.ItemsSource = list;
        }

        /**
         * @brief ウィンドウが描画された後に呼び出されます。
         * 
         * @param [in] sender ウィンドウ
         * @param [in] e イベント
         */
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // データグリッドに識別用のタグを設定します。
            var rowNum = dataGrid.Items.Count;
            var columnNum = dataGrid.Columns.Count;
            for (int i = 0; i < rowNum; ++i)
            {
                for (int j = 0; j < columnNum; ++j)
                {
                    var cell = GetDataGridCell(dataGrid, i, j);
                    if (cell == null)
                    {
                        continue;
                    }
                    var tag = string.Format("{0:00}{1:00}", i, j);
                    cell.Tag = tag;
                }
            }
        }

        /**
         * データグリッドでマウスの左ボタンが押下された時に呼び出されます。
         * 
         * @param [in] sender データグリッド
         * @param [in] e マウスボタンイベント
         */
        private void DataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // ボタンが押下されたセルのタグを表示します。
            var dataGrid = sender as DataGrid;
            if (dataGrid == null)
            {
                return;
            }
            var point = e.GetPosition(dataGrid);
            var cell = GetDataGridCell<DataGridCell>(dataGrid, point);
            Console.WriteLine($"down={cell.Tag}");
        }

        /**
         * データグリッドでマウスの左ボタンが放された時に呼び出されます。
         * 
         * @param [in] sender データグリッド
         * @param [in] e マウスボタンイベント
         */
        private void DataGrid_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // ボタンが放されたセルのタグを表示します。
            var dataGrid = sender as DataGrid;
            if (dataGrid == null)
            {
                return;
            }
            var point = e.GetPosition(dataGrid);
            var cell = GetDataGridCell<DataGridCell>(dataGrid, point);
            Console.WriteLine($"up={cell.Tag}");
        }

        // 内部メソッド(詳細は省略します)

        private DataGridCell GetDataGridCell(DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var row = GetDataGridRow(dataGrid, rowIndex);
            if (row == null)
            {
                return null;
            }

            var presenter = GetVisualChild<DataGridCellsPresenter>(row);
            if (presenter == null)
            {
                return null;
            }

            var generator = presenter.ItemContainerGenerator;
            var cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            if (cell == null)
            {
                dataGrid.UpdateLayout();
                var column = dataGrid.Columns[columnIndex];
                dataGrid.ScrollIntoView(row, column);
                cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            }
            return cell;
        }

        private DataGridRow GetDataGridRow(DataGrid dataGrid, int index)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var generator = dataGrid.ItemContainerGenerator;
            var row = generator.ContainerFromIndex(index) as DataGridRow;
            if (row == null)
            {
                dataGrid.UpdateLayout();
                var item = dataGrid.Items[index];
                dataGrid.ScrollIntoView(item);
                row = generator.ContainerFromIndex(index) as DataGridRow;
            }
            return row;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T result = default(T);
            var count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                var child = VisualTreeHelper.GetChild(parent, i) as Visual;
                result = child as T;
                if (result != null)
                {
                    break;
                }

                result = GetVisualChild<T>(child);
            }
            return result;
        }

        private T GetDataGridCell<T>(DataGrid dataGrid, Point point)
        {
            T result = default(T);
            var hitResultTest = VisualTreeHelper.HitTest(dataGrid, point);
            if (hitResultTest != null)
            {
                var visualHit = hitResultTest.VisualHit;
                while (visualHit != null)
                {
                    if (visualHit is T)
                    {
                        result = (T)(object)visualHit;
                        break;
                    }
                    visualHit = VisualTreeHelper.GetParent(visualHit);
                }
            }
            return result;
        }
    }
}
